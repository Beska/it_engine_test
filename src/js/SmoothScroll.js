class SmoothScroll {
  constructor() {
    this.links = document.querySelectorAll(".primary-nav ul a");
    this.events();
  }

  events() {
    for (const link of this.links) {
      link.addEventListener('click', this.scrollEvent.bind(this));
    }
  }

  scrollEvent(e) {
    e.preventDefault();

    const href = e.target.getAttribute("href");
    const offsetTop = document.querySelector(href).offsetTop;

    scroll({
      top: offsetTop,
      behavior: "smooth"
    })
  }
}

export default SmoothScroll;