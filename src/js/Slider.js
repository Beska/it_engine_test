import $ from 'jquery';

class Slider {
  constructor() {
    this.slider = $('.testimonial');
    this.slide = $('.testimonial__box');
    this.itemWidth = this.slide.outerWidth();
    this.prevBtn = $('#prev-btn');
    this.nextBtn = $('#next-btn');
    this.showPrevSlide();
    this.showNextSlide();
  }

  showNextSlide() {
    this.nextBtn.on("click", (e) => {
      e.preventDefault();

      this.slider.children("div:last").prependTo(this.slider);
      this.slider.css("left", -this.itemWidth);

      this.slider.animate(
        {
          left: 0
        },
        300,
        "linear"
      );
    });
  }

  showPrevSlide() {
    this.prevBtn.on("click", (e) => {
      e.preventDefault();

      this.slider.animate(
        {
          left: -this.itemWidth
        },
        300,
        "linear",
        () => {
          this.slider.children("div:first").appendTo(this.slider);
          this.slider.css("left", 0);
        }
      );
    });
  }
}

export default Slider;