class Accordion {
  constructor() {
    this.questions = document.querySelectorAll('.faq__question');
    this.events();
  }

  events() {
    this.questions.forEach(question => question.addEventListener("click", () => {

      if (question.parentNode.classList.contains("active")) {
        question.parentNode.classList.toggle("active")
      }

      else {
        this.questions.forEach(question => question.parentNode.classList.remove("active"))
        question.parentNode.classList.add("active")
      }
    }))
  }
}

export default Accordion;