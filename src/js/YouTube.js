class YouTube {
  constructor() {
    this.youtubeBtn = document.querySelector('.youtube-btn');
    this.videoContent = document.querySelector('.video__content');
    this.videoYT = document.querySelector('.video__yt');
    this.events();
  }

  events() {
    this.youtubeBtn.addEventListener('click', this.playVideo.bind(this));
  }

  playVideo(e) {
    e.preventDefault();

    this.videoContent.style.display = 'none';
    this.videoYT.src += "?autoplay=1";

    setTimeout(() => {
      this.videoYT.style.display = 'block';
    }, 400);
  }
}

export default YouTube;