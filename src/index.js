import Accordion from './js/Accordion';
import SmoothScroll from './js/SmoothScroll';
import YouTube from './js/YouTube';
import LazyLoading from './js/LazyLoading';
import StickyHeader from './js/StickyHeader';
import Slider from './js/Slider';

import './scss/styles.scss';
import '@fortawesome/fontawesome-free/js/all';

const accordion = new Accordion();
const smoothScroll = new SmoothScroll();
const youTube = new YouTube();
const lazyLoading = new LazyLoading();
const stickyHeader = new StickyHeader();
const slider = new Slider();