const path = require("path");

module.exports = {
  entry: "./src/index.js",
  module: {
    rules: [
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
          options: {
            esModule: false,
            sources: {
              list: [
                {
                  tag: 'img',
                  attribute: 'data-lazy',
                  type: 'src',
                },
                {
                  tag: 'img',
                  attribute: 'src',
                  type: 'src',
                },
              ]
            }
          }
        }
      },
      {
        test: /\.(png|jpe?g|gif)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[hash].[ext]",
            outputPath: "imgs"
          }
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: "[name].[hash].[ext]",
            outputPath: "fonts"
          }
        },
      },
    ]
  }
};