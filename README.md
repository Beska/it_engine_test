## Single page responsive website done with html/scss/js

> Project configuration done with webpack. <br> For testing purposes use production version ( index.html within dist folder )

## Getting Started

<p><strong>1.</strong> Clone the repository</p>

<div class="highlight highlight-source-shell"><pre>git clone https://gitlab.com/Beska/it_engine_test/</pre></div>

<p><strong>2.</strong> Navigate to it_engine_test directory</p>

<div class="highlight highlight-source-shell"><pre>cd it_engine_test</pre></div>

<p><strong>3.</strong> Install dependencies <code>package.json</code></p>

<div class="highlight highlight-source-shell"><pre>npm i</pre></div>


### Up and Running

<p><strong>1.</strong> Run the development server</p>

<div class="highlight highlight-source-shell"><pre>npm run start</pre></div>


<p><strong>2.</strong> Generate production-ready files</p>

<div class="highlight highlight-source-shell"><pre>npm run build</pre></div>

<p><strong>That's it!</strong> <g-emoji class="g-emoji" alias="raised_hands" fallback-src="https://github.githubassets.com/images/icons/emoji/unicode/1f64c.png"><img class="emoji" alt="raised_hands" height="20" width="20" src="https://github.githubassets.com/images/icons/emoji/unicode/1f64c.png"></g-emoji></p>
